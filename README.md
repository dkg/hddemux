Given restrictive firewalls and intrusive network monitors, it can be
both privacy-preserving and connectivity-enhancing to be able to serve
both HTTPS (HTTP/1.x) and DNS-over-TLS from the same TCP port.

This project aims to document specifically how that can be done
safely, and to provide simple code to demonstrate the mechanism.

As of this writing, the code is deployed and in-use at the host
`dns.cmrg.net`, which offers both HTTPS and a full recursive resolver
(via DNS-over-TLS), both on port 443.  Please see https://dns.cmrg.net
for more details of that service.

This project includes:

 * `hddemux.c` -- a `libuv`-based HTTP/1.x and DNS multiplexing server
 
 * `hddemux.socket` and `hddemux.service` -- systemd units to manage
   `hddemux`

 * `draft-dkg-dprive-demux-dns-http.md` -- an Internet Draft
   documenting the rationale and the algorithm.

Patches and commentary welcome!
